import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CampianComponent } from './campian/campian.component';
import {RouterModule} from '@angular/router';
import {CampaignsListComponent} from './campians-list/campaigns-list.component';
import {HttpClientModule} from '@angular/common/http';
import { NewCampiagnComponent } from './new-campiagn/new-campiagn.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CampaignsListComponent,
    CampianComponent,
    NewCampiagnComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {path: 'campaigns', component: CampaignsListComponent},
      {path: 'campaigns/new', component: NewCampiagnComponent},
      {path: '**',  redirectTo: 'campaigns', pathMatch: 'full'},
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
