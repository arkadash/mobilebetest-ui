import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Campaign} from '../Types';
import {CampaignsRestService} from '../campaigns-rest.service';

@Component({
  selector: 'app-new-campiagn',
  templateUrl: './new-campiagn.component.html',
  styleUrls: ['./new-campiagn.component.css']
})
export class NewCampiagnComponent implements OnInit {

  campaign: Campaign = {
    id: 0,
    name: '',
    thresholds: {
      max_total: null,
      max_per_user: null
    }
  };
  pageTitle = 'New Campaign';
  constructor(
    private _service: CampaignsRestService,
    private _router: Router
  ) { }

  ngOnInit() {
  }


  addCampaign() {
    this._service.createCampaign(this.campaign)
      .subscribe(res => {
        this.navigateCampaigns();
      }, );
  }

  navigateCampaigns() {
    this._router.navigate(['/campaigns']);
  }
}
