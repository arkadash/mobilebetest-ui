import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCampiagnComponent } from './new-campiagn.component';

describe('NewCampiagnComponent', () => {
  let component: NewCampiagnComponent;
  let fixture: ComponentFixture<NewCampiagnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCampiagnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCampiagnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
