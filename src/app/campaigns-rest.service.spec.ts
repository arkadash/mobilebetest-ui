import { TestBed, inject } from '@angular/core/testing';

import { CampaignsRestService } from './campaigns-rest.service';

describe('CampaignsRestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CampaignsRestService]
    });
  });

  it('should be created', inject([CampaignsRestService], (service: CampaignsRestService) => {
    expect(service).toBeTruthy();
  }));
});
