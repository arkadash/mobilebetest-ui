import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampianComponent } from './campian.component';

describe('CampianComponent', () => {
  let component: CampianComponent;
  let fixture: ComponentFixture<CampianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
