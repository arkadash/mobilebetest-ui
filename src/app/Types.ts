export interface Campaign {
  id: number;
  name: string;
  current?: number;
  thresholds: {
    max_total: number;
    max_per_user: number;
  };
}
