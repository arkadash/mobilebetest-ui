import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Campaign} from './Types';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CampaignsRestService {

  constructor(
    private _http: HttpClient
  ) { }
  private static API_ENDPOINT = '';

  private static getHeaders() {
    return new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    });
  }
  getCampaigns(): Observable<Campaign[]> {
    const headers = CampaignsRestService.getHeaders();
    return this._http.get<Campaign[]>
    (CampaignsRestService.API_ENDPOINT + '/campaigns/all', {headers}).pipe(
      tap(data => console.log('campaigns result: ' + JSON.stringify(data))), );
  }

  getCampaignsCurrent(): Observable<any> {
    const headers = CampaignsRestService.getHeaders();
    return this._http.get<any>
    (CampaignsRestService.API_ENDPOINT + '/campaigns/max', {headers}).pipe(
      tap(data => console.log('campaigns max result: ' + JSON.stringify(data))), );
  }

  createCampaign(camp: Campaign): Observable<Campaign> {
    const headers = CampaignsRestService.getHeaders();
    return this._http.post<Campaign>
    (CampaignsRestService.API_ENDPOINT + '/campaigns', camp, {headers}).pipe(
      tap(data => console.log('Workouts result: ' + JSON.stringify(data))), );
  }
}
