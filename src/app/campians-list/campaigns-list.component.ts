import { Component, OnInit } from '@angular/core';
import {Campaign} from '../Types';
import {CampaignsRestService} from '../campaigns-rest.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-campaigns-list',
  templateUrl: './campaigns-list.component.html',
  styleUrls: ['./campaigns-list.component.css']
})
export class CampaignsListComponent implements OnInit {

  campaigns: Campaign[];
  pageTitle = 'Campaigns';
  constructor(
    private _service: CampaignsRestService,
    private _router: Router,
  ) { }

  ngOnInit() {
   this.getAllCampaigns();
  }
  getAllCampaigns() {
    this._service.getCampaigns()
      .subscribe(res => {
        this.campaigns = res;
        this.getCampaignsCurrent();
      }, );
  }

  getCampaignsCurrent() {
    this._service.getCampaignsCurrent()
      .subscribe(idsMap => {
        this.campaigns.forEach(value => {
          value.current = idsMap[value.id];
        });
      }, );
  }

  addCampaign() {
    this._router.navigate(['/campaigns/new']);
  }

  onRowClick(id) {
   alert('Not yet Implemented, Should show id: ' + id);
  }


}
